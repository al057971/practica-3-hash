package diccionario;
import java.util.Scanner;

public class Diccionario {
private final int[] nueva_tabla_con_indices = new int [10];
private final int[] palabras_en_Asscii = new int [10];
    public static void main(String[] args) {
        Diccionario tabla = new Diccionario();
        String[] palabras = new String[10];
        palabras[0] = "manzana";
        palabras[1] = "gato";
        palabras[2] = "matematicas";
        palabras[3] = "perro";
        palabras[4] = "espacio";   
        palabras[5] = " ";
        palabras[6] = " ";
        palabras[7] = " ";
        palabras[8] = " ";
        palabras[9] = " "; 
        
        String[] definicion = new String[10];
        definicion[0] = """
                        La manzana es el fruto comestible 
                        de la especie Malus domestica, el manzano com\u00fan. 
                        Es una fruta pom\u00e1cea de forma redonda y sabor muy dulce, 
                        dependiendo de la variedad""";
        definicion[1] = """
                        Mam\u00edfero felino de tama\u00f1o generalmente peque\u00f1o, cuerpo flexible, cabeza redonda, 
                        patas cortas, cola larga, pelo espeso y suave, largos bigotes 
                        y u\u00f1as retr\u00e1ctiles; es carn\u00edvoro y tiene gran agilidad,
                         buen olfato, buen o\u00eddo y excelente visi\u00f3n nocturna; 
                        existen muchas especies diferentes.""";
        definicion[2] = """
                        Ciencia que estudia las propiedades de los n\u00fameros y 
                        las relaciones que se establecen entre ellos.""";
        definicion[3] = """
                        Mam\u00edfero carn\u00edvoro dom\u00e9stico de la familia de los c\u00e1nidos que se caracteriza 
                        por tener los sentidos del olfato y el o\u00eddo muy finos, por su inteligencia y
                         por su fidelidad al ser humano, que lo ha domesticado desde tiempos prehist\u00f3ricos;
                         hay much\u00edsimas razas, de caracter\u00edsticas muy diversas.""";
        definicion[4] = """
                        Medio f\u00edsico en el que se sit\u00faan los cuerpos y los movimientos, 
                        y que suele caracterizarse como homog\u00e9neo, continuo, tridimensional e ilimitado.""";
        definicion[5] = "";
        definicion[6] = "";
        definicion[7] = "";
        definicion[8] = "";
        definicion[9] = "";
        
        System.out.println("palabras");
        
        for(int i = 0; i< palabras.length; i++){
            System.out.println(palabras[i]);
        }
        System.out.println("numero asscii de palabras");
        tabla.modeloAsscii(palabras);
        System.out.println("numero de indices");
        tabla.insertar(palabras);
        
        System.out.println("digite el numero para realizar cualquier opcion");
        System.out.println("oprima (1) para insertar una palabra");
        System.out.println("oprima (2) para buscar la palabra");
        System.out.println("oprima (3) para salir");
        System.out.print("opcion: ");
        boolean bandera = false;
        
        while(bandera == false){
        
        Scanner sc = new Scanner(System.in);
        int opcion = sc.nextInt();
        
            int palabra = 5;
            int def_palabra = 5;
            if(opcion == 1){
                Scanner c = new Scanner(System.in);
                System.out.print("ingrese la palabra: ");
                String ingresado = c.nextLine();  
                palabra = palabra + 1;
                palabras[palabra-1] = ingresado;
                System.out.println("nueva palabra ingresada correctamente");
                
                Scanner d = new Scanner(System.in);
                System.out.print("ingrese la deficicion de la palabra: ");
                String def = d.nextLine();
                def_palabra= def_palabra + 1;
                definicion[def_palabra-1] = def;
                System.out.println("la definicion se añadio correctamente");
                
                for(int i = 0; i< palabras.length; i++){
                System.out.println(palabras[i]);
                }
                System.out.println("numero asscii de palabras");
                tabla.modeloAsscii(palabras);
                System.out.println("numero de indices");
                tabla.insertar(palabras);
            
                 }
                if(opcion == 2){    
                    boolean encontrado = false;
                    Scanner g = new Scanner(System.in);
            for (String palabra1 : palabras) {
                System.out.println(palabra1);
            }
                    System.out.print("ingrese la palabra a buscar: ");
                    String dato_buscar = g.nextLine();          
                    for(int i = 0; i < palabras.length; i++){
                        if (dato_buscar.equals(palabras[i])){
                            System.out.println("       --------**--------");
                            System.out.println("se encontro la palabra: " + dato_buscar+"");
                            System.out.println("y tiene la definicion siguiente: \n\n"+definicion[i]);
                            System.out.println("       --------**--------\n");
                            encontrado = true;
                            break;
                        }  
                        
                    } 
                    if(encontrado == false){
                        System.out.println("\nNo se encontro la palabra: " + dato_buscar+"\n");
                    }
                }
                
                if (opcion == 3){
                    bandera = true;
                    break;
                }
                
                System.out.println("digite el numero para realizar cualquier opcion");
                System.out.println("oprima (1) para insertar una palabra");
                System.out.println("oprima (2) para buscar la palabra");
                System.out.println("oprima (3) para salir");
            }
        
    }
     
    //numero primo constante no necesariamente sercano a una potencia de 2
    private final Integer size = 13;
    
    //constructor nulo
    public Diccionario(){
        
    }
    
    //metodo que asigna una key
    public int hash(int k){
        int key = 0;
        //el 0x7fffffff es q el hash genere numeros positivos
        key = (k & 0x7fffffff)% size;        
        return key;
    }
    
    public Integer buscar(Integer k){
        return null;
    }
    
    //metodos para insertar indices al arreglo
    public void insertar(String[] arreglo){
       int[] tablita_comp = new int[nueva_tabla_con_indices.length]; 
       
        for(int i = 0; i < palabras_en_Asscii.length; i++){
            nueva_tabla_con_indices[i] = hash(palabras_en_Asscii[i]);
            tablita_comp[i] = nueva_tabla_con_indices[i];
            //manejo de coliciones
            for(int k = 0; k < tablita_comp.length; k++){
                if(nueva_tabla_con_indices[i] == tablita_comp[k]){  
                    while(nueva_tabla_con_indices[i] != tablita_comp[k] ){         
                        nueva_tabla_con_indices[i] -=1;
                   }
                   while(nueva_tabla_con_indices[i] > 10){
                       nueva_tabla_con_indices[i] -=1;
                   }
                }
            }
            System.out.println(nueva_tabla_con_indices[i]);
        }
    
    }
    
    public void borrar(Integer k){
        
    }
    
    //convertir la primera letra de mi palabra en un asicci
    public void modeloAsscii(String[] arreglo){
     int suma = 0; 
    
       for(int i = 0; i< arreglo.length;i++){
           int palabra_tamaño = arreglo[i].length();
           int[] arre_suma = new int[arreglo[i].length()];
           for(int k = 0; k < palabra_tamaño; k++){
              char character = arreglo[i].charAt(k);
              arre_suma[k] = (int) character;     
              suma = suma + arre_suma[k];
           }
          palabras_en_Asscii[i] = suma; 
          System.out.println(palabras_en_Asscii[i]);
       }     

    }
}
